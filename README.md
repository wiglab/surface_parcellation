# Surface parcellation and parcel creation code

This matlab code runs gradient-based parcellation of fMRI timecourse data in CIFTI/GIFTI format. All subjects should be registered to a common surface space (we recommend the fs_LR 32k space for surface elements).

This code release was used in [Han et al. (2018)](https://academic.oup.com/cercor/article/28/12/4403/5126789), and is revised from the code in Gordon et al. (2016) \([code](https://sites.wustl.edu/petersenschlaggarlab/resources/), [paper](https://academic.oup.com/cercor/article/26/1/288/2367115)\). When publishing results using this code, please cite the following papers:
> - Han L, Savalia NK, Chan MY, Agres PF, Nair AS, Wig GS. 2018. Functional parcellation of the cerebral cortex across the human adult lifespan. Cereb Cortex. 28:4403-4423.
> - Gordon EM, Laumann TO, Adeyemo B, Huckins JF, Kelley WM, Petersen SE. 2016. Generation and Evaluation of a Cortical Area Parcellation from Resting-State Correlations. Cereb Cortex. 26:288-303.

For additional references see here:
> - Cohen AL, Fair DA, Dosenbach NUF, Miezin FM, Dierker D, Van Essen DC, Schlaggar BL, Petersen SE. 2008. Defining functional areas in individual human brains using resting functional connectivity MRI. Neuroimage. 41:45–57.
> - Wig GS, Laumann TO, Petersen SE. 2014. An approach for parcellating human cortical areas using resting-state correlations. Neuroimage. 93:276–291.




## Prerequisites
> These are programs/functions that must be in your paths before you run `surface_parcellation.m` and `pacel_creator.m`

- **Matlab** (with Parallel Computing Toolbox) - the code has been developed and tested with Matlab 2012b. Updating code may be necessary if different versions of Matlab to be used.

- **Connectome Workbench** - software developed by the [Human Connectome Project](http://www.humanconnectome.org/software/connectome-workbench.html) (check if `wb_command` is in your system path)

- **32k Conte69 Atlas** - atlas generated from 69 healthy adults registered to the [32k fs_LR surface mesh](http://brainvis.wustl.edu/wiki/index.php//Caret:Atlases/Conte69_Atlas) (access to this atlas requires making a manual request via email; add this atlas to Matlab path)

- **Matlab GIfTI library** - code to read/write GIfTI files by [Guillaume Flandin](https://www.artefact.tk/software/matlab/gifti/) (a version of of GIFTI functions that is compatible with Matlab 2012b is provided in `./dependency`)


## Primary functions

- `surface_parcellation.m` - in each cortical hemisphere, computes gradients for each subject, averages those gradients, smooths the average gradients on the atlas surface, and runs watershed-based edge detection on each vertex's group-average gradient map, resulting in a group-average edge (i.e., boundary) map for each vertex. The boundary maps from each vertex are averaged to create a final group-average boundary density map.

- `parcel_creator.m` - builds discrete parcels from boundary density maps, based on a threshold for separating parcels (we recommend approximately the 30-50th percentile of boundary map values, but results should be checked to ensure this does not result in over- or under-parcellation).


## Dependency

#### Gordon2016Surface_parcellation_distribute-20agwt4_mod

> These are parcellation codes downloaded from the code link in the beginning of the README, with minor modifications in some functions (e.g.,`ft_read_cifti_mod.m`)

- `cifti-matlab-master` - directory that contains CIFTI functions to read/write CIFTI files, which is based on [code](https://github.com/oostenveld/cifti-matlab) released by Robert Oostenveld, with modifications to optimize reading of fMRI CIFTI dtseries and dconn data.
- `paircorr_mod.m` - fast (but RAM-demanding) method of calculating pairwise correlations between all timecourses in a timeseries
- `FisherTransform.m` - code to apply the Fisher transformation to correlations
- `metric_minima_all.m` - code to detect local minima in gradient maps
- `watershed_algorithm_all_par.m` - code to detect edges in gradient maps by applying the watershed-by-flooding algorithm. NOTE: this code utilizes Matlab's parallel computing toolbox, and so will run much faster if that toolbox is installed
- `node_neighbors.txt` - this represents the node adjacencies of the fs_LR 32k surface. IMPORTANT: this file must be changed if using a different surface format. Node adjacencies can be generated from a surface using the Caret software (caret_command -surface-topology-neighbors)
- `cifti_template.dtseries.nii` - a CIFTI file template for generating boundary/parcellation maps

#### @gifti

- GIFTI functions that are compatible with Matlab 2012b.


## Disclaimer

This code is provided **"as is"**, without any warranty. We have tested it using Matlab 2012b. The code/libraries may need to be updated due to different software and hardware environment.
