#!/bin/bash
# This script download prerequisites and sample data for surface parcellation
# Usage: sh ./download_tool_example_data.sh <path_of_example_folder>
# 02/26/2021 LH@WigNeuroimagingLab

# the example folder of parcellation script package
outputdir="$1" # this takes in the path of example folder (like: /data/data4/LHAN/TEST/projects/surface_parcellation/example)

#---------------
# prerequisites
#---------------
# 32k fs_LR atlas
# The full package can be accessed from http://brainvis.wustl.edu/wiki/index.php//Caret:Atlases/Conte69_Atlas
# Access is granted by wustl via requests sent to sums@brainvis.wustl.edu.

# GIfTI library
# A version of @gifti compatible with Matlab2012b is already included in the ./dependency directory. 
# The updated version that is compatible with newer Matlab could be accessed from https://www.artefact.tk/software/matlab/gifti

# HCP Connectome Workbench
# find more download options here: https://www.humanconnectome.org/software/get-connectome-workbench
wget -P "$outputdir"/prereq https://www.humanconnectome.org/storage/app/media/workbench/workbench-rh_linux64-v1.5.0.zip
unzip "$outputdir"/prereq/workbench-rh_linux64-v1.5.0.zip -d "$outputdir"/prereq/


#------------------
# sample data (MSC)
#------------------
# functional data (cifti file)
name_func='sub-MSC01_ses-func01_task-rest_bold_32k_fsLR'
wget -O "$outputdir"/data/"$name_func".dtseries.nii https://openneuro.org/crn/datasets/ds000224/snapshots/1.0.3/files/derivatives:surface_pipeline:sub-MSC01:processed_restingstate_timecourses:ses-func01:cifti:"$name_func".dtseries.nii
# tmask file
wget -O "$outputdir"/data/"$name_func"_tmask.txt https://openneuro.org/crn/datasets/ds000224/snapshots/1.0.3/files/derivatives:surface_pipeline:sub-MSC01:processed_restingstate_timecourses:ses-func01:cifti:"$name_func"_tmask.txt
