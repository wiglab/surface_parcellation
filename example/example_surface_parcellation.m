% This is an example script to run surface parcellation using downloaded data
%
% Note: run download_tool_example_data.sh beforehand to obtain tools and sample data
% This script has been tested using Matlab2012b
%
% 02/26/2021 LH@WigNeuroimagingLab

% add the path of parcellation package
dir_script='/data/data4/LHAN/TEST/projects/surface_parcellation';
addpath(genpath(dir_script))

% also check whether
%   1. HCP Workbench is in the system path
%   2. 32k Conte69 atlas is in the Matlab path

%----------------------------
% step 1
% calculate the boundary maps
subjects = {'MSC01'};
dir_data = '/data/data4/LHAN/TEST/projects/surface_parcellation/example/data';
input_imagefiles = {fullfile(dir_data, 'sub-MSC01_ses-func01_task-rest_bold_32k_fsLR.dtseries.nii')};
hems = 'both';
outputdir = fullfile(dir_script, 'example', 'outputs');
input_tmaskfiles = {fullfile(dir_data, 'sub-MSC01_ses-func01_task-rest_bold_32k_fsLR_tmask.txt')};
cd(outputdir)

surface_parcellation(subjects, input_imagefiles, hems, outputdir, input_tmaskfiles);


%----------------------------
% step 2
% calculate the parcel maps
threshperc = .35;
output_filestem = 'parcel';
hems = ['L';'R'];
for hemidx = 1:size(hems,1)
    hem = hems(hemidx,1);
    edgemetricname = ['avg_corrofcorr_allgrad_' hem '_smooth2.55_wateredge_avg.func.gii'];
    parcel_creator(edgemetricname, hem, threshperc, output_filestem)
end