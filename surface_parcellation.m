function  surface_parcellation(subjects, input_imagefiles, hems, outputdir, input_tmaskfiles)
%
% Description
%
%   Generate gradient-based parcellation on surface registered subject data
%
%   This version of surface parcellation will average gradient data across
%   subjects to generate a mean gradient and edge map for the group.
% 
% 
% Input
%   subjects            :   a variable of m*1 cells with each having
%                           a subject name
%
%   input_imagefiles    :   a variable of m*n cells with each filling with full
%                           path to a functional data file.
%                           m = number of subjects, n = number of files per subject
%
%                           Two types of data file are supported: CIFTI
%                           (.dtseries.nii) and GIFTI (.func.gii).
%
%                           If CIFTI files are used here, it should be m*1
%                           cells, because each subject only have one cifti
%                           file (left and right hemispheres combined
%                           together) 
%                           If GIFTI files are used, it should be m*2 cells,
%                           because each subject should have a gifti file
%                           for each hemisphere. On each row, the 1st cell
%                           should be the functional data of left
%                           hemisphere, and 2nd cell should be the data of
%                           right hemisphere
%
%   hems                :   either 'LEFT', 'RIGHT', or 'BOTH' hemispheres
%                           be parcellated. 
%
%   outputdir           :   the folder to write results into. Omit to write
%                           into the working directory
%
%   tmasklist           :   the tmask files if scrubbed data are used. It
%                           should be m*1 cells, with each having the full
%                           path to the tmask file of each subject. Omit to
%                           use all the frames
%                           The tmask for each subject should be a binary
%                           text file of length #timepoints indicating
%                           timepoints to include (1) or censor (0)
%
% Note:
%   Please add paths of the following software/packages:
%       1. add HCP workbench to the system path
%       2. add 32k Conte69 atlas folder to the Matlab path
%
%
% TOL 01/25/13, modified by EMG 04/03/15
% Modification/update in Wig Neuroimaging Lab:
%   - 10/12/2015    LH  fixed bugs
%   - 01/05/2016    LH  accepted CIFTI files with/without medial wall
%                       vertices
%   - 07/01/2016    LH  fixed bugs in dependency functions (ft_read_cifti_mod.m
%                       and ft_write_cifti_mod.m)
%   - 09/22/2017    LH  expanded functionality of dependencies (ft_read_cifti_mod.m
%                       and ft_write_cifti_mod.m) correctly read/write
%                       data and header info in .dconn.nii file
%   - 03/01/2018    LH  accepted GIFTI files
%   - 12/17/2020    LH  cleaned up the scripts (for better organization)


% determine the path of the parcellation script
path_tmp = which('surface_parcellation.m');
if isempty(path_tmp)
    error('Please add the folder and subfolders of parcellation package to your path')
end
path_script = fileparts(path_tmp);
depend_path = fullfile(path_script, 'dependency', 'Gordon2016Surface_parcellation_distribute-20agwt4_mod');

% parameters to set
smooth = 2.55; % sigma for geodesic smoothing applied to gradient maps
corticalverts_per_hemisphere = 32492; % number of vertices per hemisphere (use 32492 for 32K_fs_LR standard)
neighborsfile = fullfile(depend_path, 'node_neighbors.txt'); % location of surface node neighbors file from caret -surface-topology-neighbors
medialwall_L  = fullfile(depend_path, 'medial_wall.L.32k_fs_LR.func.gii');
medialwall_R  = fullfile(depend_path, 'medial_wall.R.32k_fs_LR.func.gii');
cifti_template = fullfile(depend_path, 'cifti_template.dtseries.nii');

%-----------------------------------------------------------------------

% medial wall mask for both left and right hems
medialmaskdata_L = gifti(medialwall_L);
medialmaskdata_L = medialmaskdata_L.cdata;
medialmaskdata_R = gifti(medialwall_R);
medialmaskdata_R = medialmaskdata_R.cdata;
medialmaskdata = [medialmaskdata_L; medialmaskdata_R];
clear medialmaskdata_L medialmaskdata_R

if ~exist('outputdir')
    outputdir = pwd;
end
if ~exist('input_tmaskfiles')
    input_tmaskfiles = [];
end

% hemisphere names
HEMS = {'L';'R'};
hemname = {'LEFT';'RIGHT'};
hemname_low = {'left';'right'};


bufsize=16384;
% read in node neighbor file
[neighbors(:,1) neighbors(:,2) neighbors(:,3) neighbors(:,4) neighbors(:,5) neighbors(:,6) neighbors(:,7)] = ...
textread([neighborsfile],'%u %u %u %u %u %u %u','delimiter',' ','bufsize',bufsize,'emptyvalue',NaN);
neighbors = neighbors+1;


% interpret hemispheres
switch hems
    case {'LEFT' 'left' 'L' 'l'}
        h = 1;
    case {'RIGHT' 'right' 'R' 'r'}
        h = 2;
    case {'BOTH' 'both' 'B' 'b'}
        h = [1:2];
end

% make output folder
mkdir(outputdir)
cd(outputdir)

for hem = h
    
    disp(['Hemisphere :' HEMS{hem}])
    
    % altas midthickness surface
    midsurf_32k_atlas = ['Conte69.' HEMS{hem} '.midthickness.32k_fs_LR.surf.gii'];
    
    for s = 1:length(subjects)
        
        subject = subjects{s};
        
        disp(['-- Subject #' num2str(s) ': ' subject])
        
        % load functional file
        [~, ~, ext] = fileparts(input_imagefiles{s,1});
        
        switch ext
            case '.nii' % if its a CIFTI file (.dtseries.nii)
                cifti_file = input_imagefiles{s,1};
                ciftistruct = ft_read_cifti_mod(cifti_file);
                
                % toss medial wall from functional data
                % usually CIFTI data include medial wall vertices, but
                % it's not always the case, so check it first
                idx = find(ciftistruct.brainstructure==2);
                S_data = size(ciftistruct.data,1); % how many vertices in the data field
                S_stru = size(ciftistruct.brainstructure,1); % how many vertices in the structure field
                
                if ~isequal(idx(end), size(medialmaskdata,1))
                    error('The surface vertices mismatch') % the number of cortical vertices should match
                end
                
                if isequal(S_data, S_stru)
                    ciftistruct.data(medialmaskdata == 1,:) = [];
                end
                
                data_timecourse = ciftistruct.data;
                clear idx S_data S_stru
                
                
            case '.gii' % if it's a GIFTI file (.func.gii)
                data_l = gifti(input_imagefiles{s,1}); data_l = data_l.cdata; % gifti data of left hem
                data_r = gifti(input_imagefiles{s,2}); data_r = data_r.cdata; % gifti of right hem
                data_timecourse = [data_l; data_r];
                data_timecourse(medialmaskdata == 1,:) = []; % toss medial wall data
                clear data_l data_r
                
                ciftistruct = ft_read_cifti_mod(cifti_template); % load cifti template
                
        end
        
        ciftistruct.data = [];
        ciftistruct.brainstructure(medialmaskdata == 1) = -1;
        
        % set up some values to be used across subjects
        if s==1
            thishem_inds = (1:corticalverts_per_hemisphere) + (corticalverts_per_hemisphere * (hem-1));
            medial_wall = logical(ciftistruct.brainstructure(thishem_inds)==-1); % medial wall of only this hem
            fullgrads = zeros(nnz(ciftistruct.brainstructure==hem));
            cort_ind = (1:nnz(ciftistruct.brainstructure==hem)) + nnz(ciftistruct.brainstructure==(hem-1));
        end
        
        % handle tmask
        if ~isempty(input_tmaskfiles)
            tmask = load(input_tmaskfiles{s});
        else
            tmask = ones(size(data_timecourse,2),1);
        end
        
        % calculate correlation maps
        disp('---- Calculating correlation map')
        cifti_corrmap = paircorr_mod(data_timecourse(cort_ind,logical(tmask))',data_timecourse(:,logical(tmask))');
        
        % remove NaNs (produced if vertices have no data)
        cifti_corrmap(isnan(cifti_corrmap)) = 0;
        % apply the Fisher tranformation
        cifti_corrmap = FisherTransform(cifti_corrmap);
        
        % calculate correlation similarity
        disp('---- Calculating similarity map')
        corrofcorr = corrcoef(cifti_corrmap');
        
        % apply the Fisher tranformation
        corrofcorr = FisherTransform(corrofcorr);
        
        % write out corr of corr cifti file for gradient calculation   
        ciftistruct.brainstructure = ciftistruct.brainstructure(thishem_inds);
        ciftistruct.pos = ciftistruct.pos(thishem_inds,:);
        ciftistruct.hdr.dim(6:7) = size(corrofcorr);
        ciftistruct = rmfield(ciftistruct,'transform');
        ciftistruct = rmfield(ciftistruct,'dim');
        ciftistruct = rmfield(ciftistruct,'brainstructurelabel');
        ciftistruct.brainstructurelabel{1} = ['CORTEX_' hemname{hem}];
        ciftistruct.dimord = 'pos_pos';
        ciftistruct.data = corrofcorr;
        clear corrofcorr
        ft_write_cifti_mod(['corrofcorr_' HEMS{hem}],ciftistruct);
        ciftistruct.data = [];
        
        % calculate gradients
        disp('---- Calculating gradient')
        gradsname = ['corrofcorr_allgrad_' HEMS{hem}];
        system(['wb_command -cifti-gradient ' outputdir '/corrofcorr_' HEMS{hem} '.dconn.nii ROW ' outputdir '/' gradsname '.dconn.nii -' hemname_low{hem} '-surface ' midsurf_32k_atlas ]);
        
        % convert gradients and load
        grads = ft_read_cifti_mod([outputdir '/' gradsname '.dconn.nii']);
        grads = grads.data;
        
        % add subject gradients to running average
        fullgrads = [fullgrads + grads];
        clear grads
    end
    
    
    % average gradients across subjects
    fullgrads = fullgrads./length(subjects);
    
    % save out average gradients
    ciftistruct.data = fullgrads;
    gradsname = ['avg_corrofcorr_allgrad_' HEMS{hem}];
    ft_write_cifti_mod(gradsname,ciftistruct);
    ciftistruct.data = [];
    clear fullgrads
    
    % smooth gradients before edge detection
    disp('Smoothing average gradient')
    system(['wb_command -cifti-smoothing ' outputdir '/' gradsname '.dconn.nii ' num2str(smooth) ' 0 ROW ' outputdir '/' gradsname '_smooth' num2str(smooth) '.dconn.nii -' hemname_low{hem} '-surface ' midsurf_32k_atlas]);

    % load smoothed gradients
    ciftistruct = ft_read_cifti_mod([outputdir '/' gradsname '_smooth' num2str(smooth) '.dconn.nii']);
    
    % average smoothed gradients across maps (unused but useful to look at)
    ciftistruct_1D = ciftistruct;
    ciftistruct_1D.data = mean(ciftistruct.data,2);
    ft_write_cifti_mod([outputdir '/' gradsname '_smooth' num2str(smooth) 'avg'],ciftistruct_1D);
    
    % put smoothed gradients into full surface space
    fullgrads_smooth = zeros(corticalverts_per_hemisphere, nnz(medial_wall==0));
    fullgrads_smooth(~medial_wall,:) = ciftistruct.data;
    ciftistruct.data = [];
    clear grads
    
    % boundary mapping
    disp('Calculating boundary')
    % get local minima of each smoothed gradient map
    fullgrads_medial = fullgrads_smooth;
    fullgrads_medial(medial_wall,:) = 1000;
    minimametrics = metric_minima_all(fullgrads_medial,3,neighbors);
    clear fullgrads_medial
    
    % run watershed-by-flooding algorithm on each gradient map to generate boundaries
    labels = watershed_algorithm_all_par(fullgrads_smooth,minimametrics,200,1,neighbors);
    
    % average across boundary maps and save
    labels_avg = mean(labels==0,2);
    save(gifti(single(labels_avg)),[outputdir '/' gradsname '_smooth' num2str(smooth) '_wateredge_avg.func.gii']);
    
    % save watershed edges from all gradient maps
    ciftistruct.data = labels(~medial_wall,:);
    clear labels
    ft_write_cifti_mod([outputdir '/labels_' HEMS{hem}],ciftistruct);
    clear ciftistruct
    
end